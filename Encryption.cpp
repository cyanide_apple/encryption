#include<iostream>
#include "Encryption.h"
#include<string>
#include<sstream>

using namespace std;

int Encryption::encrypt(int value) {

	stringstream  ss;
	ss << value;
	string s = ss.str();

	stringstream ss2;

	for (char c : s) {

		int temp = (c - '0') + 7;
		temp %= 10;
		ss2 << temp;

	}

	s = ss2.str();
	swap(s[0], s[2]);
	swap(s[1], s[3]);

	int num = atoi(s.c_str());
	return num;
}

int Encryption::decrypt(int value) {

	stringstream  ss;
	ss << value;
	string s = ss.str();

	int diff = 4 - s.size();

	string temp = "";

	for (int i = 0; i < diff; i++) {

		temp += "0";

	}

	temp += s;
	s = temp;
	swap(s[0], s[2]);
	swap(s[1], s[3]);

	stringstream ss2;

	for (char c : s) {

		int _temp = (c - '0') - 7;
		if (_temp < 0) {
			_temp += 10;
		}
		ss2 << _temp;

	}

	s = ss2.str();

	int num = atoi(s.c_str());

	return num;

}