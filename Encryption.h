#ifndef ENCRYPTION_H
#define ENCRYPTION_H

class Encryption {

public:
	int encrypt(int);
	int decrypt(int);

};


#endif // !ENCRYPTION_H
